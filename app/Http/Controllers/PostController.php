<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\SearchData\Search;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function postList(Request $request)
    {
        $searchData = $request->search_term;
        $order_by = $request->order_by;
        $sort_by = $request->sort_by;
        $columns = ['id','post_details'];

        $query = DB::table('posts');
        Search::searchQuery($query, $columns, $searchData);
        $query->orderByRaw('' . $sort_by . ' ' . $order_by . '')->where('deleted_at', null);
        $posts = $query->paginate($request->per_page);

        foreach($posts as $post){
            $post->categories_id = json_decode($post->categories_id, true);
            foreach($post->categories_id as $category_id){
                $category = Category::where('id', $category_id)->first();
                $post->for_multiselect[] = ['id' => $category->id, 'title' => $category->title];
            }
        }

        return $posts;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category =  Post::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = [
            'categories_id' => $request->categories_id,
            'post_details' => $request->post_details
        ];
        Post::find($id)->update($updateData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::find($id)->delete();
    }
}
