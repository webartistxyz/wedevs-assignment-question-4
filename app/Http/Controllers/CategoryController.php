<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\SearchData\Search;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::latest()->select('id', 'title')->get();
    }

    public function categoryList(Request $request)
    {
        $searchData = $request->search_term;
        $order_by = $request->order_by;
        $sort_by = $request->sort_by;
        $columns = ['title', 'description'];

        $query = DB::table('categories');
        Search::searchQuery($query, $columns, $searchData);
        $query->orderByRaw('' . $sort_by . ' ' . $order_by . '')->where('deleted_at', null);
        $data = $query->paginate($request->per_page);

        return $data;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required'
        ]);
        $category =  Category::create($validatedData);
        return ['id' => $category->id, 'title' => $category->title];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'title' => 'required'
        ]);
        Category::whereId($id)->update($validatedData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countPost = Post::where('categories_id', 'LIKE', '%' . $id . '%')->count();
        if($countPost < 1){
            Category::find($id)->delete();
        }else{
            throw new \ErrorException("You can't delete it! some post have against this category");
        }
    }
}
