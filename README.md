# weDevs Assignment Question 4

## Technologies used

1.  Laravel version 7.17.2
2.  Vue Js version 2.6
3.  vue-router version 3.3.4
4.  axios version 0.19

## Getting Started

1.  Clone the repository

    `https://gitlab.com/webartistxyz/wedevs-assignment-question-4`

2.  Install all dependencies

    `composer install`

3.  Run migrations to setup database

    First create a database in phpmyadmin and configure with .env file and then run the command below

    `php artisan migrate`

4.  Start the server

    `php artisan serve`
