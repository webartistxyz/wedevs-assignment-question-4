import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        component: require("./views/Categories/CategoryList").default,
        meta: { title: "Category" }
    },
    {
        path: "/posts",
        component: require("./views/Posts/PostList").default,
        meta: { title: "Category" }
    },
    // {
    //     path: "/change-pass",
    //     component: require("./views/Profile/ChangePassword").default,
    //     meta: { title: "Chage Password" }
    // },

    //redirect
    { path: "*", redirect: "/" }
];

export default new VueRouter({
    mode: "history",
    routes
});
