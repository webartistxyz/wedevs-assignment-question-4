export const category = {
    data() {
        return {
            modalFor: false,
            isOpen: false
        };
    },
    computed: {},
    methods: {
        setAddForm() {
            this.modalFor = false;
            this.isOpen = true;
            this.formData = {};
        },
        editIt(data) {
            this.isOpen = true;
            this.formData = data;
            this.modalFor = true;
        },
        onSubmit() {
            this.save("/category").then(response => {
                if (response.status) this.isOpen = false;
            });
        },
        onSubmitUpdate(id) {
            this.update(`/category/${id}`).then(response => {
                if (response.status) this.isOpen = false;
            });
        }
    }
};
