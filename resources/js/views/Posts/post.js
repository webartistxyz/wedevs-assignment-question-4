export const post = {
    data() {
        return {
            modalFor: false,
            isOpen: false,
            isOpenCategoryModal: false,
            categoryForm: {},
            categories_id: [],
            options: []
        };
    },
    computed: {},
    methods: {
        setAddForm() {
            this.getAllCategories();
            this.modalFor = false;
            this.isOpen = true;
            this.categories_id = [];
            this.formData = {};
        },
        setCategoryModal() {
            this.isOpenCategoryModal = true;
        },
        getAllCategories() {
            this.getData("get", "/category").then(response => {
                this.options = response.data;
            });
        },
        saveNewCategory() {
            this.saveCustom("/category", this.categoryForm).then(response => {
                if (response.status) {
                    this.isOpenCategoryModal = false;
                    this.categories_id.push(response.data);
                    this.options.push(response.data);
                    this.categoryForm = {};
                }
            });
        },
        editIt(data) {
            this.getAllCategories();
            this.isOpen = true;
            this.categories_id = data.for_multiselect;
            this.formData = data;
            this.modalFor = true;
        },
        filterCategoryId(categories) {
            let newArr = [];
            categories.forEach(category => {
                newArr.push(category.id);
            });
            this.formData.categories_id = JSON.stringify(newArr);
        },
        onSubmit() {
            this.filterCategoryId(this.categories_id);
            setTimeout(() => {
                this.save("/post_route").then(response => {
                    if (response.status) this.isOpen = false;
                });
            }, 0.05);
        },
        onSubmitUpdate(id) {
            this.filterCategoryId(this.categories_id);
            //console.log(id);
            this.update(`/post_route/${id}`).then(response => {
                if (response.status) this.isOpen = false;
            });
        }
    }
};
