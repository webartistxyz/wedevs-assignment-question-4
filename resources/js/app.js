require("./bootstrap");

window.Vue = require("vue");
import router from "./routes";

//app mixins
import FormFunction from "./Mixins/FormFunction";
Vue.mixin(FormFunction);

//import all components
require("./components/allComponents");

Vue.component("MainIndex", require("./views/Home/Index.vue").default);

const app = new Vue({
    el: "#app",
    router,
    data: {
        buttonDisable: false
    },
    computed: {},
    methods: {
        onSubmit(id) {
            document.getElementById(id).click();
        },
        closeModal(id) {
            document.getElementById(id).click();
        }
    },
    mounted() {}
});
