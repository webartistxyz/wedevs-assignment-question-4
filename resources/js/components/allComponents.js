Vue.component("list-data", require("./ListData/List").default);
Vue.component("modal", require("./Modals/Modal").default);
Vue.component("category-modal", require("./Modals/CategoryModal").default);
Vue.component("app-button", require("./Buttons/Button").default);
Vue.component("app-submit-button", require("./Buttons/SubmitButton").default);
Vue.component("app-input", require("./Inputs/AppInput").default);
Vue.component(
    "close-modal-button",
    require("./Buttons/CloseModalButton").default
);
//sweet alert
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
Vue.use(VueSweetalert2);
