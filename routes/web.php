<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/category_list', 'CategoryController@categoryList')->name('category_list');
Route::post('/post_list', 'PostController@postList')->name('post_list');

Route::resources([
    'category' => 'CategoryController',
    'post_route' => 'PostController'
]);




Route::get('/{vue_capture?}', function () {



    return view('welcome');

})->where('vue_capture', '[\/\w\.-]*');
